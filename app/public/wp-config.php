<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cH6kvTOELNNhH2F/1jRygHJPkfeU5qf7p6RNPTlLYhRjrlI44GGM1zKuf3BSfkuR5K/og47HM9AUjvbmHFLfvA==');
define('SECURE_AUTH_KEY',  'JLNknCHWuewR4k8xuKCKFM8vMm8FHavI6XwvReL078POykvOgV5XmGxA5yimbaw6GjgpdTYRi45gSFkAndIigQ==');
define('LOGGED_IN_KEY',    'JzIb++WcBfs/5t07/yyI38RsCsGtnqgcU//neiDc2kH+7r/QS6c7EGQX1xZdtdG4T1QPBdITnZkVlKqAa9ffaw==');
define('NONCE_KEY',        '5T2s2beyo+YoZr6Rzu5CD9L2nr3tQK0DnfxQW9Xm/7vqA2K7hBRygcNWx3jIDNNgFBAqtAUJg/3kywD+3edPAA==');
define('AUTH_SALT',        'eYKzsg2fFuOVQfMCx/NtItxm355lw4hXif8P/V1xdiOx4IjyYFiUnXw7yH+f6Xtgo9zNh0wydAupgQedP9bmhg==');
define('SECURE_AUTH_SALT', 'Xdq0cPJaWqU9MU5Ae/WZ0Fuc50dQq4kDDFGKD2R5JN6lDcf5MIe3k+6KtVJLximunw7LBmDGpYEXFFxYNClP1A==');
define('LOGGED_IN_SALT',   '32yGSkIzh+EOQdOVXXKqmieM9vC7HFjH+xkkn3zOdkowMOikI6BLEJ9JmX5kPJx7Y6mA0h3s2F57J/ecL8vFeQ==');
define('NONCE_SALT',       'kkWd+1O1+7Pl5F7bJl0ZPSw8QGWOdxZKsLQGYCaiDefsgJNYz7gc+Mbr7W57f8XzF36o9XEMnVQmdSeEV/F8eQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
