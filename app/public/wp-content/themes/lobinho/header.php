<!DOCTYPE html>
<html lang="pt-br">


<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <title>IN Júnior</title>
    <?php wp_head(); ?>
</head>


<body>

    <header>
        <section class="header-content">
        <?php
            $args = array(
                'menu' => 'navegacao',
                'container' => true, 
                'menu_class' => 'header-content'
            );
            wp_nav_menu($args);
        ?>
            <!-- <div>
                <h2 class="sublinhado header-h2"><a href="/listaDeLobinhos.php">Nossos lobinhos</a></h2>
            </div>

            <figure><a href="index.html"><img class="accessMain" src="/img/Logo.svg"></figure></a>

            <div>
                <h2 class="sublinhado header-h2"><a href="/quemsomos.php">Quem somos?</a></h2>   
            </div> -->
        </section>
    </header>