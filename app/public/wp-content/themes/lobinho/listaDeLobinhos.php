<?php 
    // Template Name: listadelobinhos
?>
    
    <?php get_header(); ?>

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style-listaDeLobinhosstyle-body.css">

    <main>
        <section class="all-forms">
            <form>
                <input type="text">
                <button class="addLobo" onclick="goToAdicionarLobinho()">+Lobo</button>
            </form>

            <form class="checkbox">
                <input type="checkbox" id="filter" name="filter" value="filterActive">
                <label for="filter">Ver lobinhos adotados</label>
            </form>
        </section>

        <div class="lobo0">
            <a class="lobo-picture-0" href="mostrarLobinho.html"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/image_1.png"></a>

            <div class="cardContent">
                <div class="cardName">
                    <div class="name-button-0">
                        <h3>Nome do Lobo</h3>
                        <button class="botaoAdotar">Adotar</button>
                    </div>
                    <p>Idade: XX anos</p>
                </div>

                <div class="cardDescription0">
                    <p>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                        do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                        faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
                        Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                        do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                        faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
                </div>

            </div>

        </div>

        <div class="lobo1">
            <div class="lobo-picture-1"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/image_2.png"></div>

            <div class="cardContent">
                <div class="cardName">
                    <div class="name-button-1">
                        <h3>Nome do Lobo</h3>
                        <button class="botaoAdotar">Adotar</button>
                        
                    </div>
                    <p>Idade: XX anos</p>
                </div>

                <div class="cardDescription1">
                    <p>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                        do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                        faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
                        Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                        do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                        faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
                </div>
            </div>
            
        </div>          

        <div class="lobo0">
            <div class="lobo-picture-0"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/image_1.png"></div>

            <div class="cardContent">
                <div class="cardName">
                    <div class="name-button-0">
                        <h3>Nome do Lobo</h3>
                        <button class="botaoAdotar">Adotar</button>
                    </div>
                    
                    <p>Idade: XX anos</p>
                </div>

                <div class="cardDescription0">
                    <p>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                        do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                        faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
                        Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                        do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                        faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
                </div>

            </div>

        </div>

        <div class="lobo1">
            <div class="lobo-picture-1"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/image_2.png"></div>

            <div class="cardContent">
                <div class="cardName">
                    <div class="name-button-1">
                        <h3>Nome do Lobo</h3>
                        <button class="botaoAdotar">Adotar</button>
                        
                    </div>
                    <p>Idade: XX anos</p>
                </div>

                <div class="cardDescription1">
                    <p>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                        do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                        faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
                        Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                        do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                        faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
                </div>
            </div>
            
        </div>
    </main>

    <script src="ajax-listaDeLobinhos.js"></script>
    
    <?php get_footer(); ?> 