<?php get_header(); ?>

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style-adotar.css">
    
    <main>
        <section>

            <div class="adoption-wolf-info">

                <figure class="wolf-circle-container"><img src="AdoteUmLobinho/image_1.png"></figure>

                <div class="adoption-wolf-txt">
                    <h1>Adote o(a) NomeDoLobo</h1>
                    <p>ID: 314159</p>
                </div>
            </div>

            <form class="adoption-forms">
                <div class="name-age-form">
                    <div>
                        <label for="nome">Seu nome:</label>
                        <input type="text" id="nome">
                    </div>
                    <div>
                        <label for="idade">Idade:</label>
                        <input type="number" id="idade">
                    </div>
                </div>

                <div class="email-form">
                    <label for="email">E-mail:</label>
                    <input type="email" id="email">
                </div>

                <button id="botaoAdotar">Adotar</button>
            </form>

        </section>
    </main>
    
    <?php get_footer(); ?> 