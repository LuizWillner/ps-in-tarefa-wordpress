<?php get_header(); ?>

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style-adicionarLobinho-body.css">

    <main>
        <section>
            <div id="cabecalhoForm"><h1>Coloque um Lobinho para Adoção</h1></div>
            <form>
                <div>
                    <div>
                        <label for="nomeDoLobo">Nome do Lobinho:</label>
                        <input type="text" id="nomeDoLobo">
                    </div>
                    <div>
                        <label for="idade">Anos:</label>
                        <input type="number" id="idade">
                    </div>
                </div>
                <label for="linkFoto">Link da foto:</label>
                <input type="text" id="linkFoto">
                <label for="descricao">Descrição:</label>
                <textarea id="descricao"></textarea>
            </form>
            <button id="botaoSalvar">Salvar</button>
        </section>
    </main>

    <script src="ajax-adicionarLobinho.js"></script>

    <?php get_footer(); ?> 
