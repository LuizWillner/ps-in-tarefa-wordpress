    <footer>

        <section class="footer-1">

            <div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15575.049134517309!2d-43.13790676899171!3d-22.905166322717495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ee1756031%3A0xd1dcbde0df6f873c!2sAv.%20Milton%20Tavares%20de%20Souza%20-%20Boa%20Viagem%2C%20Niter%C3%B3i%20-%20RJ%2C%2024210-346!5e0!3m2!1spt-BR!2sbr!4v1632967710302!5m2!1spt-BR!2sbr" width="350" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>

            <div class="all-info">
                <div class="info">
                    <figure><img src="<?php echo get_stylesheet_directory_uri() ?>/img/pin.png" class="icon"></figure>
                    <p class="info-txt">Av. Milton Tavares de Sousa, s/n - Sala 115 B - Boa Viagem, Niterói - RJ, 24210-315</p>
                </div>

                <div class="info">
                    <figure><img src="<?php echo get_stylesheet_directory_uri() ?>/img/telephone.png" class="icon"></figure>
                    <p class="info-txt">(99)99999-9999</p>
                </div>

                <div class="info">
                    <figure><img src="<?php echo get_stylesheet_directory_uri() ?>/img/mail.png" class="icon"></figure>
                    <p class="info-txt">salve-lobos@lobINnhos.com</p>
                </div>

                <div class="info">
                    <h3 id="quemsomos"><a href="quemsomos.html">Quem somos</a></h3>
                </div>
            </div>

        </section>

        <section class="footer-2">
            <p>Desenvolvido com</p>
            <figure><img src="<?php echo get_stylesheet_directory_uri() ?>/img/paws.svg"</figure>
        </section>

    </footer>

<?php wp_footer(); ?>
</body>

</html>