<?php get_header(); ?>
 
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style-index-body.css">

    <main>
        <section class="first-section">
            <div class="large-txt-container">
                <h1 class="section1-txt" id="titleSection1Txt">Adote um Lobinho</h1>
            </div>
            <div class="small-txt-container">
                <hr>
                <div><p class="section1-txt" id="bodySection1Txt">É claro que o consenso sobre a necessidade de qualificação apresenta tendências no
                sentido de aprovar a manutenção das regras de conduta normativas.</p></div>
            </div>

        </section>
        
        <section class="second-section">
            <h2 class="subtitle" id="titleSection2Txt">Sobre</h2>
            <div>
                <p class="section2-txt" id="bodySection2Txt">Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
                Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual
                faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
            </div>
        </section>

        <section class="third-section">
            <h2 class="subtitle">Valores</h2>

            <div class="all-valores">

                <div class="valor">
                    <div class="img-valores"><figure><img src="<?php echo get_stylesheet_directory_uri() ?>/img/life-insurance.svg" class="teste"></figure></div>
                    <h3 class="title-valores">Proteção</h3>
                    <p class="description-valores">Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a
                        criação do sistema de participação geral.</p>
                </div>

                <div class="valor">
                    <figure><img src="<?php echo get_stylesheet_directory_uri() ?>/img/care.svg" class="img-valores"></figure>
                    <h3 class="title-valores">Carinho</h3>
                    <p class="description-valores">Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a
                        criação do sistema de participação geral.</p>
                </div>

                <div class="valor">
                    <figure><img src="<?php echo get_stylesheet_directory_uri() ?>/img/Group.svg" class="img-valores"></figure>
                    <h3 class="title-valores">Companheirismo</h3>
                    <p class="description-valores">Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a
                        criação do sistema de participação geral.</p>
                </div>

                <div class="valor">
                    <figure><img src="<?php echo get_stylesheet_directory_uri() ?>/img/Rescue Dog.svg" class="img-valores"></figure>
                    <h3 class="title-valores">Resgate</h3>
                    <p class="description-valores">Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a
                        criação do sistema de participação geral.</p>
                </div>
            
            </div>

        </section>

        <section class="fourth-section">
            <h2 class="subtitle">Lobos Exemplo</h2>

            <div class="all-lobos">
                <div class="lobo0">
                    <div class="lobo-picture-0"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/image_1.png"></div>

                    <div class="cardContent">
                        <div class="cardName">
                            <h3>Nome do Lobo</h3>
                            <p>Idade: XX anos</p>
                        </div>

                        <div class="cardDescription0">
                            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                                <?php the_title(); ?>
                                <?php the_content(); ?>
                            <?php endwhile; else: ?>
                                <p>desculpe, o post não segue os critérios escolhidos</p>
                        <?php endif; ?>
                        </div>

                    </div>

                </div>
            

                <div class="lobo1">
                    <div class="lobo-picture-1"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/image_2.png"></div>

                    <div class="cardContent">
                        <div class="cardName">
                            <h3>Nome do Lobo</h3>
                            <p>Idade: XX anos</p>
                        </div>

                        <div class="cardDescription1">
                            <p>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                                do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                                faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
                                Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
                                do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
                                faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>

    </main>

    <script src="ajax-index.js"></script>

    <?php get_footer(); ?> 