const url = "http://lobinhos.herokuapp.com/";
const complement = "wolves/";

const queryString = window.location.search;
console.log("teste");
console.log(queryString);

let id = queryString.split("?id=")[1];
console.log(id);
const complement2 = `${id}/`

const main = document.querySelector("main")
console.log(main);

function fillPage(wolfName, wolfPic, wolfDescription) {
    let newElement = document.createElement("section");
    console.log(newElement);
    newElement.classList.add("section-main");

    newElement.innerHTML = `
    <h1>${wolfName}</h1>

    <section class="card-show-lobinho">

        <div class="foto-adotar-excluir">
            <figure><img id="imgAndshadow" src="${wolfPic}"></figure>
            <div class="botoes-content">
                <button class="botao-add-excluir" id="verde" onclick="goToAdotarLobinho()">Adotar</button>
            <button class="botao-add-excluir" onclick="deleteWolf()">Excluir</button>
            </div>
        </div>

        <div class="descricao">
            <p>${wolfDescription}</p>
        </div>
    </section>`

    main.appendChild(newElement);
}

function deleteWolf() {
    let fetchConfig = {
        method:"DELETE",
    }

    fetch(url+complement+complement2, fetchConfig)
    .then(alert("Lobo excluído com sucesso!"))
    .then(window.history.back())
    .catch(error => console.warn(error));
}

function goToAdotarLobinho() {
    window.location.replace(`adotarlobinho.html?id=${id}`);
    //window.open(`adotarlobinho.html?id=${id}`)
}




// ============== GET ========================
fetch(url+complement+complement2)
.then(resp => { return resp.json()})
.then(wolf => {
    fillPage(wolf["wolf"].name, wolf["wolf"].link_image, wolf["wolf"].description);
})
.catch(error => console.log(error));


//        <section class="section-main">
//             <h1>NomeDoLobo</h1>

//             <section class="card-show-lobinho classe2">

//                 <div class="foto-adotar-excluir">
//                     <figure><img img id="imgAndshadow" src="AdoteUmLobinho/image_1.png"></figure>
//                     <div class="botoes-content">
//                         <button class="botao-add-excluir" id="verde">Adotar</button>
//                         <button class="botao-add-excluir">Excluir</button>
//                     </div>
//                 </div>

//                 <div class="descricao">
//                     <p>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
//                         do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
//                         faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
//                         Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
//                         do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
//                         faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
//                     <br>
//                     <p>Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
//                         do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
//                         faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
//                         Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento
//                         do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual
//                         faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
//                 </div>
//             </section>
//         </section>




// GET wolves/id_do_lobo
// 	Caso o lobo esteja para adoção, irá mostrar só as informações do lobo para adoção, no formato explicado acima. 
// 	Se o lobo já tiver sido adotado, também irá retornar informações sobre o usuário que o adotou.

// "adoption": {
//       "id": 2,
//       "wolf_id": 2,
//       "name": "Rafael",
//       "age": 10,
//       "email": "rafael@id.uff.br",
//       "created_at": "2021-10-01T15:11:50.130Z",
//       "updated_at": "2021-10-01T15:11:50.130Z"
// }
